package com.ata.appbundle.utils

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.ata.appbundle.R
import com.ata.appbundle.data.source.local.model.Film
import com.ata.appbundle.view.DetailActivity

class FilmAdapter(type: Int) : RecyclerView.Adapter<FilmAdapter.ListViewHolder>() {
    private var listFilm = ArrayList<Film>()
    private val filmType = type

    fun setFilms(films: List<Film>) {
        if (films == null) return
        this.listFilm.clear()
        this.listFilm.addAll(films)
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ListViewHolder {
        val view: View = LayoutInflater.from(viewGroup.context).inflate(R.layout.item_row_film, viewGroup, false)
        return ListViewHolder(view)
    }

    override fun getItemCount(): Int {
        return listFilm.size
    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        val film = listFilm[position]
        Glide.with(holder.itemView.context)
            .load(film.poster)
            .into(holder.imgPhoto)
        holder.tvName.text = film.title
        holder.tvDetail.text = film.genre

        holder.itemView.setOnClickListener {
            val detailActivity = Intent(holder.itemView.context, DetailActivity::class.java)
            var extras = Bundle()
            extras.putString(DetailActivity.EXTRA_TITLE, film.title)
            extras.putInt(DetailActivity.EXTRA_TYPE, filmType)
            detailActivity.putExtras(extras)
            holder.itemView.context.startActivity(detailActivity)
        }
    }

    class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvName: TextView = itemView.findViewById(R.id.tv_item_name)
        var tvDetail: TextView = itemView.findViewById(R.id.tv_item_detail)
        var imgPhoto: ImageView = itemView.findViewById(R.id.img_item_photo)
    }


}