package com.ata.appbundle.utils

import android.content.Context
import com.ata.appbundle.data.source.remote.response.FilmResponse
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.util.*

class JsonHelper(private val context: Context) {

    private fun parsingFileToString(fileName: String): String? {
        return try {
            val `is` = context.assets.open(fileName)
            val buffer = ByteArray(`is`.available())
            `is`.read(buffer)
            `is`.close()
            String(buffer)

        } catch (ex: IOException) {
            ex.printStackTrace()
            null
        }
    }

    fun loadMovies(): List<FilmResponse> {
        val list = ArrayList<FilmResponse>()
        try {
            val responseObject = JSONObject(parsingFileToString("movies.json").toString())
            val listArray = responseObject.getJSONArray("movies")
            for (i in 0 until listArray.length()) {
                val movie = listArray.getJSONObject(i)

                val title = movie.getString("title")
                val poster = movie.getString("poster")
                val genre = movie.getString("genre")
                val runningTime = movie.getString("running_time")
                val originalLanguage = movie.getString("original_language")
                val overview = movie.getString("overview")

                val filmResponse = FilmResponse(title, poster, genre, runningTime, originalLanguage, overview)
                list.add(filmResponse)
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        return list
    }

    fun loadTvShows(): List<FilmResponse> {
        val list = ArrayList<FilmResponse>()
        try {
            val responseObject = JSONObject(parsingFileToString("tvshows.json").toString())
            val listArray = responseObject.getJSONArray("tvshows")
            for (i in 0 until listArray.length()) {
                val movie = listArray.getJSONObject(i)

                val title = movie.getString("title")
                val poster = movie.getString("poster")
                val genre = movie.getString("genre")
                val runningTime = movie.getString("running_time")
                val originalLanguage = movie.getString("original_language")
                val overview = movie.getString("overview")

                val filmResponse = FilmResponse(title, poster, genre, runningTime, originalLanguage, overview)
                list.add(filmResponse)
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
        return list
    }
}

