package com.ata.appbundle.utils

import com.ata.appbundle.data.source.local.model.Film
import com.ata.appbundle.data.source.remote.response.FilmResponse

object DataDummy {
    fun generateDummyMovies(): List<Film> {
        val movies = ArrayList<Film>()

        movies.add(Film("Alita: Battle Angel (2019)", "https://image.tmdb.org/t/p/w300/xRWht48C2V8XNfzvPehyClOvDni.jpg", "Action, Science Fiction, Adventure", "2h 2m",
            "English", "When Alita awakens with no memory of who she is in a future world she does not recognize, she is taken in by Ido, a compassionate doctor who realizes that somewhere in this abandoned cyborg shell is the heart and soul of a young woman with an extraordinary past."))

        movies.add(Film("Aquaman (2018)", "https://image.tmdb.org/t/p/w300/wJhqsBTBrWHcuLfU5kHF4Ar4HOL.jpg", "Action, Adventure, Fantasy", "2h 24m",
            "English", "Once home to the most advanced civilization on Earth, Atlantis is now an underwater kingdom ruled by the power-hungry King Orm. With a vast army at his disposal, Orm plans to conquer the remaining oceanic people and then the surface world. Standing in his way is Arthur Curry, Orm's half-human, half-Atlantean brother and true heir to the throne."))

        movies.add(Film("Cold Pursuit (2019)", "https://image.tmdb.org/t/p/w300/cO802woIgZsLbOWPUSsleobKyDp.jpg", "Action, Crime, Thriller", "1h 59m",
            "English", "The quiet family life of Nels Coxman, a snowplow driver, is upended after his son's murder. Nels begins a vengeful hunt for Viking, the drug lord he holds responsible for the killing, eliminating Viking's associates one by one. As Nels draws closer to Viking, his actions bring even more unexpected and violent consequences, as he proves that revenge is all in the execution."))

        movies.add(Film("Fantastic Beasts: The Crimes of Grindelwald (2018)", "https://image.tmdb.org/t/p/w300/2MokCu61LzWw872lPzq1VCJXTD1.jpg", "Adventure, Fantasy, Drama", "2h 14m",
            "English", "Gellert Grindelwald has escaped imprisonment and has begun gathering followers to his cause—elevating wizards above all non-magical beings. The only one capable of putting a stop to him is the wizard he once called his closest friend, Albus Dumbledore. However, Dumbledore will need to seek help from the wizard who had thwarted Grindelwald once before, his former student Newt Scamander, who agrees to help, unaware of the dangers that lie ahead. Lines are drawn as love and loyalty are tested, even among the truest friends and family, in an increasingly divided wizarding world."))

        movies.add(Film("Avengers: Infinity War (2018)", "https://image.tmdb.org/t/p/w300/1uoU4qPZEcOus0rQf8Tz4JCn3cX.jpg", "Adventure, Action, Science Fiction", "2h 29m",
            "English", "As the Avengers and their allies have continued to protect the world from threats too large for any one hero to handle, a new danger has emerged from the cosmic shadows: Thanos. A despot of intergalactic infamy, his goal is to collect all six Infinity Stones, artifacts of unimaginable power, and use them to inflict his twisted will on all of reality. Everything the Avengers have fought for has led up to this moment - the fate of Earth and existence itself has never been more uncertain."))

        movies.add(Film("Mary Queen of Scots (2018)", "https://image.tmdb.org/t/p/w300/xzG8TDGEj1yl1gHSnJzOrOM3WNG.jpg", "Drama, History", "2h 4m",
            "English", "In 1561, Mary Stuart, widow of the King of France, returns to Scotland, reclaims her rightful throne and menaces the future of Queen Elizabeth I as ruler of England, because she has a legitimate claim to the English throne. Betrayals, rebellions, conspiracies and their own life choices imperil both Queens. They experience the bitter cost of power, until their tragic fate is finally fulfilled."))

        movies.add(Film("Master Z: Ip Man Legacy (2018)", "https://image.tmdb.org/t/p/w300/9GlO5a0iOUUgFG50IqkP0vLSfS5.jpg", "Action", "1h 47m",
            "Cantonese", "Following his defeat by Master Ip, Cheung Tin Chi tries to make a life with his young son in Hong Kong, waiting tables at a bar that caters to expats. But it's not long before the mix of foreigners, money, and triad leaders draw him once again to the fight."))

        movies.add(Film("Mortal Engines (2018)", "https://image.tmdb.org/t/p/w300/9KZYABU1zCtU7tB3mQJiocHwCc8.jpg", "Adventure, Fantasy", "2h 9m",
            "English", "Many thousands of years in the future, Earth’s cities roam the globe on huge wheels, devouring each other in a struggle for ever diminishing resources. On one of these massive traction cities, the old London, Tom Natsworthy has an unexpected encounter with a mysterious young woman from the wastelands who will change the course of his life forever."))

        movies.add(Film("Robin Hood (2018)", "https://image.tmdb.org/t/p/w300/AiRfixFcfTkNbn2A73qVJPlpkUo.jpg", "Adventure, Action, Thriller", "1h 56m",
            "English", "A war-hardened Crusader and his Moorish commander mount an audacious revolt against the corrupt English crown."))

        movies.add(Film("T-34 (2018)", "https://image.tmdb.org/t/p/w300/jqBIHiSglRfNxjh1zodHLa9E7zW.jpg", "War, Action, Drama, History", "2h 19m",
            "Russian", "In 1944, a courageous group of Russian soldiers managed to escape from German captivity in a half-destroyed legendary T-34 tank. Those were the times of unforgettable bravery, fierce fighting, unbreakable love, and legendary miracles."))

        return movies
    }

    fun generateDummyTVSeries(): List<Film> {
        val tvSeries = ArrayList<Film>()

        tvSeries.add(Film("Arrow (2012)", "https://image.tmdb.org/t/p/w300/gKG5QGz5Ngf8fgWpBsWtlg5L2SF.jpg", "Crime, Drama, Mystery, Action & Adventure", "42m",
            "English", "Spoiled billionaire playboy Oliver Queen is missing and presumed dead when his yacht is lost at sea. He returns five years later a changed man, determined to clean up the city as a hooded vigilante armed with a bow."))

        tvSeries.add(Film("Fairy Tail (2009)", "https://image.tmdb.org/t/p/w300/jsYTctFnK8ewomnUgcwhmsTkOum.jpg", "Action & Adventure, Animation, Comedy, Sci-Fi & Fantasy", "25m",
            "Japanese", "Lucy is a 17-year-old girl, who wants to be a full-fledged mage. One day when visiting Harujion Town, she meets Natsu, a young man who gets sick easily by any type of transportation. But Natsu isn't just any ordinary kid, he's a member of one of the world's most infamous mage guilds: Fairy Tail."))

        tvSeries.add(Film("The Flash (2014)", "https://image.tmdb.org/t/p/w300/4YaerJEQtWrAMPwjDZArSxhL3fC.jpg", "Drama, Sci-Fi & Fantasy", "44m",
            "English", "After a particle accelerator causes a freak storm, CSI Investigator Barry Allen is struck by lightning and falls into a coma. Months later he awakens with the power of super speed, granting him the ability to move through Central City like an unseen guardian angel. Though initially excited by his newfound powers, Barry is shocked to discover he is not the only \"meta-human\" who was created in the wake of the accelerator explosion -- and not everyone is using their new powers for good. Barry partners with S.T.A.R. Labs and dedicates his life to protect the innocent. For now, only a few close friends and associates know that Barry is literally the fastest man alive, but it won't be long before the world learns what Barry Allen has become...The Flash."))

        tvSeries.add(Film("Game of Thrones (2011)", "https://image.tmdb.org/t/p/w300/44rw2cOQrgUthZMhp3eibpXVy9p.jpg", "Sci-Fi & Fantasy, Drama, Action & Adventure, Mystery", "1h",
            "English", "Seven noble families fight for control of the mythical land of Westeros. Friction between the houses leads to full-scale war. All while a very ancient evil awakens in the farthest north. Amidst the war, a neglected military order of misfits, the Night's Watch, is all that stands between the realms of men and icy horrors beyond."))

        tvSeries.add(Film("Gotham (2014)", "https://image.tmdb.org/t/p/w300/4XddcRDtnNjYmLRMYpbrhFxsbuq.jpg", "Drama, Fantasy, Crime", "43m",
            "English", "Everyone knows the name Commissioner Gordon. He is one of the crime world's greatest foes, a man whose reputation is synonymous with law and order. But what is known of Gordon's story and his rise from rookie detective to Police Commissioner? What did it take to navigate the multiple layers of corruption that secretly ruled Gotham City, the spawning ground of the world's most iconic villains? And what circumstances created them – the larger-than-life personas who would become Catwoman, The Penguin, The Riddler, Two-Face and The Joker?"))

        tvSeries.add(Film("Grey's Anatomy (2005)", "https://image.tmdb.org/t/p/w300/mgOZSS2FFIGtfVeac1buBw3Cx5w.jpg", "Drama", "43m",
            "English", "Follows the personal and professional lives of a group of doctors at Seattle’s Grey Sloan Memorial Hospital."))

        tvSeries.add(Film("NCIS (2003)", "https://image.tmdb.org/t/p/w300/fi8EvaWtL5CvoielOjjVvTr7ux3.jpg", "Action & Adventure, Crime, Drama", "45m",
            "English", "From murder and espionage to terrorism and stolen submarines, a team of special agents investigates any crime that has a shred of evidence connected to Navy and Marine Corps personnel, regardless of rank or position."))

        tvSeries.add(Film("Riverdale (2017)", "https://image.tmdb.org/t/p/w300/6zBWSuYW3Ps1nTfeMS8siS4KUaA.jpg", "Drama, Mystery", "45m",
            "English", "Set in the present, the series offers a bold, subversive take on Archie, Betty, Veronica and their friends, exploring the surreality of small-town life, the darkness and weirdness bubbling beneath Riverdale’s wholesome facade."))

        tvSeries.add(Film("Shameless (2011)", "https://image.tmdb.org/t/p/w300/m2gf7SYOq9z30Q1dJFMF51DfrmF.jpg", "Drama, Comedy", "1h",
            "English", "Chicagoan Frank Gallagher is the proud single dad of six smart, industrious, independent kids, who without him would be... perhaps better off. When Frank's not at the bar spending what little money they have, he's passed out on the floor. But the kids have found ways to grow up in spite of him. They may not be like any family you know, but they make no apologies for being exactly who they are."))

        tvSeries.add(Film("Supergirl (2015)", "https://image.tmdb.org/t/p/w300/vqBsgL9nd2v04ZvCqPzwtckDdFD.jpg", "Action, Adventure, Drama, Science Fiction", "42m",
            "Russian", "Twenty-four-year-old Kara Zor-El, who was taken in by the Danvers family when she was 13 after being sent away from Krypton, must learn to embrace her powers after previously hiding them. The Danvers teach her to be careful with her powers, until she has to reveal them during an unexpected disaster, setting her on her journey of heroism."))

        return tvSeries
    }

    fun generateRemoteDummyMovies(): List<FilmResponse> {
        val movies = ArrayList<FilmResponse>()

        movies.add(
            FilmResponse("Alita: Battle Angel (2019)", "https://image.tmdb.org/t/p/w300/xRWht48C2V8XNfzvPehyClOvDni.jpg", "Action, Science Fiction, Adventure", "2h 2m",
            "English", "When Alita awakens with no memory of who she is in a future world she does not recognize, she is taken in by Ido, a compassionate doctor who realizes that somewhere in this abandoned cyborg shell is the heart and soul of a young woman with an extraordinary past.")
        )

        movies.add(
            FilmResponse("Aquaman (2018)", "https://image.tmdb.org/t/p/w300/wJhqsBTBrWHcuLfU5kHF4Ar4HOL.jpg", "Action, Adventure, Fantasy", "2h 24m",
            "English", "Once home to the most advanced civilization on Earth, Atlantis is now an underwater kingdom ruled by the power-hungry King Orm. With a vast army at his disposal, Orm plans to conquer the remaining oceanic people and then the surface world. Standing in his way is Arthur Curry, Orm's half-human, half-Atlantean brother and true heir to the throne.")
        )

        movies.add(
            FilmResponse("Cold Pursuit (2019)", "https://image.tmdb.org/t/p/w300/cO802woIgZsLbOWPUSsleobKyDp.jpg", "Action, Crime, Thriller", "1h 59m",
            "English", "The quiet family life of Nels Coxman, a snowplow driver, is upended after his son's murder. Nels begins a vengeful hunt for Viking, the drug lord he holds responsible for the killing, eliminating Viking's associates one by one. As Nels draws closer to Viking, his actions bring even more unexpected and violent consequences, as he proves that revenge is all in the execution.")
        )

        movies.add(
            FilmResponse("Fantastic Beasts: The Crimes of Grindelwald (2018)", "https://image.tmdb.org/t/p/w300/2MokCu61LzWw872lPzq1VCJXTD1.jpg", "Adventure, Fantasy, Drama", "2h 14m",
            "English", "Gellert Grindelwald has escaped imprisonment and has begun gathering followers to his cause—elevating wizards above all non-magical beings. The only one capable of putting a stop to him is the wizard he once called his closest friend, Albus Dumbledore. However, Dumbledore will need to seek help from the wizard who had thwarted Grindelwald once before, his former student Newt Scamander, who agrees to help, unaware of the dangers that lie ahead. Lines are drawn as love and loyalty are tested, even among the truest friends and family, in an increasingly divided wizarding world.")
        )

        movies.add(
            FilmResponse("Avengers: Infinity War (2018)", "https://image.tmdb.org/t/p/w300/1uoU4qPZEcOus0rQf8Tz4JCn3cX.jpg", "Adventure, Action, Science Fiction", "2h 29m",
            "English", "As the Avengers and their allies have continued to protect the world from threats too large for any one hero to handle, a new danger has emerged from the cosmic shadows: Thanos. A despot of intergalactic infamy, his goal is to collect all six Infinity Stones, artifacts of unimaginable power, and use them to inflict his twisted will on all of reality. Everything the Avengers have fought for has led up to this moment - the fate of Earth and existence itself has never been more uncertain.")
        )

        movies.add(
            FilmResponse("Mary Queen of Scots (2018)", "https://image.tmdb.org/t/p/w300/xzG8TDGEj1yl1gHSnJzOrOM3WNG.jpg", "Drama, History", "2h 4m",
            "English", "In 1561, Mary Stuart, widow of the King of France, returns to Scotland, reclaims her rightful throne and menaces the future of Queen Elizabeth I as ruler of England, because she has a legitimate claim to the English throne. Betrayals, rebellions, conspiracies and their own life choices imperil both Queens. They experience the bitter cost of power, until their tragic fate is finally fulfilled.")
        )

        movies.add(
            FilmResponse("Master Z: Ip Man Legacy (2018)", "https://image.tmdb.org/t/p/w300/9GlO5a0iOUUgFG50IqkP0vLSfS5.jpg", "Action", "1h 47m",
            "Cantonese", "Following his defeat by Master Ip, Cheung Tin Chi tries to make a life with his young son in Hong Kong, waiting tables at a bar that caters to expats. But it's not long before the mix of foreigners, money, and triad leaders draw him once again to the fight.")
        )

        movies.add(
            FilmResponse("Mortal Engines (2018)", "https://image.tmdb.org/t/p/w300/9KZYABU1zCtU7tB3mQJiocHwCc8.jpg", "Adventure, Fantasy", "2h 9m",
            "English", "Many thousands of years in the future, Earth’s cities roam the globe on huge wheels, devouring each other in a struggle for ever diminishing resources. On one of these massive traction cities, the old London, Tom Natsworthy has an unexpected encounter with a mysterious young woman from the wastelands who will change the course of his life forever.")
        )

        movies.add(
            FilmResponse("Robin Hood (2018)", "https://image.tmdb.org/t/p/w300/AiRfixFcfTkNbn2A73qVJPlpkUo.jpg", "Adventure, Action, Thriller", "1h 56m",
            "English", "A war-hardened Crusader and his Moorish commander mount an audacious revolt against the corrupt English crown.")
        )

        movies.add(
            FilmResponse("T-34 (2018)", "https://image.tmdb.org/t/p/w300/jqBIHiSglRfNxjh1zodHLa9E7zW.jpg", "War, Action, Drama, History", "2h 19m",
            "Russian", "In 1944, a courageous group of Russian soldiers managed to escape from German captivity in a half-destroyed legendary T-34 tank. Those were the times of unforgettable bravery, fierce fighting, unbreakable love, and legendary miracles.")
        )

        return movies
    }

    fun generateRemoteDummyTVSeries(): List<FilmResponse> {
        val tvSeries = ArrayList<FilmResponse>()

        tvSeries.add(
            FilmResponse("Arrow (2012)", "https://image.tmdb.org/t/p/w300/gKG5QGz5Ngf8fgWpBsWtlg5L2SF.jpg", "Crime, Drama, Mystery, Action & Adventure", "42m",
            "English", "Spoiled billionaire playboy Oliver Queen is missing and presumed dead when his yacht is lost at sea. He returns five years later a changed man, determined to clean up the city as a hooded vigilante armed with a bow.")
        )

        tvSeries.add(
            FilmResponse("Fairy Tail (2009)", "https://image.tmdb.org/t/p/w300/jsYTctFnK8ewomnUgcwhmsTkOum.jpg", "Action & Adventure, Animation, Comedy, Sci-Fi & Fantasy", "25m",
            "Japanese", "Lucy is a 17-year-old girl, who wants to be a full-fledged mage. One day when visiting Harujion Town, she meets Natsu, a young man who gets sick easily by any type of transportation. But Natsu isn't just any ordinary kid, he's a member of one of the world's most infamous mage guilds: Fairy Tail.")
        )

        tvSeries.add(
            FilmResponse("The Flash (2014)", "https://image.tmdb.org/t/p/w300/4YaerJEQtWrAMPwjDZArSxhL3fC.jpg", "Drama, Sci-Fi & Fantasy", "44m",
            "English", "After a particle accelerator causes a freak storm, CSI Investigator Barry Allen is struck by lightning and falls into a coma. Months later he awakens with the power of super speed, granting him the ability to move through Central City like an unseen guardian angel. Though initially excited by his newfound powers, Barry is shocked to discover he is not the only \"meta-human\" who was created in the wake of the accelerator explosion -- and not everyone is using their new powers for good. Barry partners with S.T.A.R. Labs and dedicates his life to protect the innocent. For now, only a few close friends and associates know that Barry is literally the fastest man alive, but it won't be long before the world learns what Barry Allen has become...The Flash.")
        )

        tvSeries.add(
            FilmResponse("Game of Thrones (2011)", "https://image.tmdb.org/t/p/w300/44rw2cOQrgUthZMhp3eibpXVy9p.jpg", "Sci-Fi & Fantasy, Drama, Action & Adventure, Mystery", "1h",
            "English", "Seven noble families fight for control of the mythical land of Westeros. Friction between the houses leads to full-scale war. All while a very ancient evil awakens in the farthest north. Amidst the war, a neglected military order of misfits, the Night's Watch, is all that stands between the realms of men and icy horrors beyond.")
        )

        tvSeries.add(
            FilmResponse("Gotham (2014)", "https://image.tmdb.org/t/p/w300/4XddcRDtnNjYmLRMYpbrhFxsbuq.jpg", "Drama, Fantasy, Crime", "43m",
            "English", "Everyone knows the name Commissioner Gordon. He is one of the crime world's greatest foes, a man whose reputation is synonymous with law and order. But what is known of Gordon's story and his rise from rookie detective to Police Commissioner? What did it take to navigate the multiple layers of corruption that secretly ruled Gotham City, the spawning ground of the world's most iconic villains? And what circumstances created them – the larger-than-life personas who would become Catwoman, The Penguin, The Riddler, Two-Face and The Joker?")
        )

        tvSeries.add(
            FilmResponse("Grey's Anatomy (2005)", "https://image.tmdb.org/t/p/w300/mgOZSS2FFIGtfVeac1buBw3Cx5w.jpg", "Drama", "43m",
            "English", "Follows the personal and professional lives of a group of doctors at Seattle’s Grey Sloan Memorial Hospital.")
        )

        tvSeries.add(
            FilmResponse("NCIS (2003)", "https://image.tmdb.org/t/p/w300/fi8EvaWtL5CvoielOjjVvTr7ux3.jpg", "Action & Adventure, Crime, Drama", "45m",
            "English", "From murder and espionage to terrorism and stolen submarines, a team of special agents investigates any crime that has a shred of evidence connected to Navy and Marine Corps personnel, regardless of rank or position.")
        )

        tvSeries.add(
            FilmResponse("Riverdale (2017)", "https://image.tmdb.org/t/p/w300/6zBWSuYW3Ps1nTfeMS8siS4KUaA.jpg", "Drama, Mystery", "45m",
            "English", "Set in the present, the series offers a bold, subversive take on Archie, Betty, Veronica and their friends, exploring the surreality of small-town life, the darkness and weirdness bubbling beneath Riverdale’s wholesome facade.")
        )

        tvSeries.add(
            FilmResponse("Shameless (2011)", "https://image.tmdb.org/t/p/w300/m2gf7SYOq9z30Q1dJFMF51DfrmF.jpg", "Drama, Comedy", "1h",
            "English", "Chicagoan Frank Gallagher is the proud single dad of six smart, industrious, independent kids, who without him would be... perhaps better off. When Frank's not at the bar spending what little money they have, he's passed out on the floor. But the kids have found ways to grow up in spite of him. They may not be like any family you know, but they make no apologies for being exactly who they are.")
        )

        tvSeries.add(
            FilmResponse("Supergirl (2015)", "https://image.tmdb.org/t/p/w300/vqBsgL9nd2v04ZvCqPzwtckDdFD.jpg", "Action, Adventure, Drama, Science Fiction", "42m",
            "Russian", "Twenty-four-year-old Kara Zor-El, who was taken in by the Danvers family when she was 13 after being sent away from Krypton, must learn to embrace her powers after previously hiding them. The Danvers teach her to be careful with her powers, until she has to reveal them during an unexpected disaster, setting her on her journey of heroism.")
        )

        return tvSeries
    }
}