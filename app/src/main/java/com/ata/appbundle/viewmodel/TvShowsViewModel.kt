package com.ata.appbundle.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.ata.appbundle.data.source.FilmRepository
import com.ata.appbundle.data.source.local.model.Film

class TvShowsViewModel(private val filmRepository: FilmRepository) : ViewModel() {
    fun getTvShows(): LiveData<List<Film>> = filmRepository.getAllTvShows()
}