package com.ata.appbundle.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.ata.appbundle.data.source.FilmRepository
import com.ata.appbundle.data.source.local.model.Film

class DetailViewModel(private val filmRepository: FilmRepository) : ViewModel() {
    private lateinit var film: LiveData<Film>
    private lateinit var title: String
    private var type: Int = 0

    fun setTitle(title: String) {
        this.title = title
    }

    fun setType(type: Int) {
        this.type = type
    }

    fun getDetail(): LiveData<Film> {
        when (type) {
            1 -> film = filmRepository.getMovieByTitle(title)
            2 -> film = filmRepository.getTvShowByTitle(title)
        }
        return film
    }

}