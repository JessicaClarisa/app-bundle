package com.ata.appbundle.di

import android.content.Context

import com.ata.appbundle.data.source.FilmRepository
import com.ata.appbundle.data.source.remote.RemoteDataSource
import com.ata.appbundle.utils.JsonHelper

object Injection {
    fun provideRepository(context: Context): FilmRepository {

        val remoteRepository = RemoteDataSource.getInstance(JsonHelper(context))

        return FilmRepository.getInstance(remoteRepository)
    }
}
