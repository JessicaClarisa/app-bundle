package com.ata.appbundle.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.ata.appbundle.R
import com.ata.appbundle.utils.FilmAdapter
import com.ata.appbundle.viewmodel.MoviesViewModel
import com.ata.appbundle.viewmodel.ViewModelFactory
import kotlinx.android.synthetic.main.fragment_movies.*


class MoviesFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_movies, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (activity != null) {
            val factory = ViewModelFactory.getInstance(requireActivity())
            val viewModel = ViewModelProvider(this, factory)[MoviesViewModel::class.java]

            val filmAdapter = FilmAdapter(1)
            progressBar.visibility = View.VISIBLE
            viewModel.getMovies().observe(this, { films ->
                progressBar.visibility = View.GONE
                filmAdapter.setFilms(films)
                filmAdapter.notifyDataSetChanged()
            })

            with(rv_movies) {
                layoutManager = LinearLayoutManager(context)
                setHasFixedSize(true)
                adapter = filmAdapter
            }
        }
    }
}