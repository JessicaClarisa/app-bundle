package com.ata.appbundle.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.ata.appbundle.R
import com.ata.appbundle.utils.TabAdapter
import com.google.android.material.tabs.TabLayoutMediator
import kotlinx.android.synthetic.main.activity_film.*

class FilmActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_film)
        initTabs()
    }

    private fun initTabs() {
        viewpager2.adapter = TabAdapter(this)
        TabLayoutMediator(tabs, viewpager2,
            TabLayoutMediator.TabConfigurationStrategy { tab, position ->
                when (position) {
                    0 -> tab.text = "Movies"
                    1 -> tab.text = "TV Shows"
                }
            }).attach()
    }
}