package com.ata.appbundle.view

import android.annotation.SuppressLint
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Layout
import android.view.View
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.ata.appbundle.R
import com.ata.appbundle.viewmodel.DetailViewModel
import com.ata.appbundle.viewmodel.ViewModelFactory
import kotlinx.android.synthetic.main.activity_detail.*

class DetailActivity : AppCompatActivity() {
    companion object {
        const val EXTRA_TITLE = "selected_title"
        const val EXTRA_TYPE = "selected_type"
    }

    @SuppressLint("WrongConstant")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        val factory = ViewModelFactory.getInstance(this)
        val viewModel = ViewModelProvider(this, factory)[DetailViewModel::class.java]
        val extras = intent.extras
        if (extras != null) {
            val title = extras.getString(EXTRA_TITLE)
            val type = extras.getInt(EXTRA_TYPE)

            val actionbar = supportActionBar
            if (actionbar != null) {
                actionbar.title = title
                actionbar.setDisplayHomeAsUpEnabled(true)
            }

            if (title != null) {
                viewModel.setTitle(title)
                viewModel.setType(type)

                progressBar.visibility = View.VISIBLE
                ln1.visibility = View.GONE
                ln2.visibility = View.GONE
                viewModel.getDetail().observe(this, { film ->
                    progressBar.visibility = View.GONE
                    ln1.visibility = View.VISIBLE
                    ln2.visibility = View.VISIBLE
                    Glide.with(applicationContext)
                        .load(film?.poster)
                        .into(findViewById(R.id.poster))

                    titleDetail.text = film?.title
                    genre.text = film?.genre
                    running_time.text = film?.running_time
                    original_lang.text = film?.original_language
                    overview.text = film?.overview

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        overview.justificationMode = Layout.JUSTIFICATION_MODE_INTER_WORD
                    }
                })
            }
        }

    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}