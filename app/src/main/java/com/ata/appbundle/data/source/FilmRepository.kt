package com.ata.appbundle.data.source

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.ata.appbundle.data.source.local.model.Film
import com.ata.appbundle.data.source.remote.RemoteDataSource
import com.ata.appbundle.data.source.remote.response.FilmResponse

class FilmRepository private constructor(private val remoteDataSource: RemoteDataSource) : FilmDataSource {
    companion object {
        @Volatile
        private var instance: FilmRepository? = null

        fun getInstance(remoteData: RemoteDataSource): FilmRepository =
            instance ?: synchronized(this) {
                instance ?: FilmRepository(remoteData)
            }
    }

    override fun getAllMovies(): LiveData<List<Film>> {
        val movieResults = MutableLiveData<List<Film>>()

        remoteDataSource.getAllMovies(object : RemoteDataSource.LoadMoviesCallback {
            override fun onAllMoviesReceived(movieResponses: List<FilmResponse>) {
                val movieList = ArrayList<Film>()
                for (response in movieResponses) {
                    val movie = Film(response.title,
                        response.poster,
                        response.genre,
                        response.running_time,
                        response.original_language,
                        response.overview)
                    movieList.add(movie)
                }
                movieResults.postValue(movieList)
            }

        })

        return movieResults
    }

    override fun getMovieByTitle(movieTitle: String): LiveData<Film> {
        val movieResult = MutableLiveData<Film>()

        remoteDataSource.getAllMovies(object : RemoteDataSource.LoadMoviesCallback {
            override fun onAllMoviesReceived(movieResponses: List<FilmResponse>) {
                for (response in movieResponses) {
                    if (response.title == movieTitle) {
                        val movie = Film(
                            response.title,
                            response.poster,
                            response.genre,
                            response.running_time,
                            response.original_language,
                            response.overview
                        )
                        movieResult.postValue(movie)
                    }
                }
            }
        })
        return movieResult
    }


    override fun getAllTvShows(): LiveData<List<Film>> {
        val tvShowResults = MutableLiveData<List<Film>>()

        remoteDataSource.getAllTvShows(object : RemoteDataSource.LoadTvShowsCallback {
            override fun onAllTvShowsReceived(tvShowResponses: List<FilmResponse>) {
                val tvShowList = ArrayList<Film>()
                for (response in tvShowResponses) {
                    val movie = Film(response.title,
                        response.poster,
                        response.genre,
                        response.running_time,
                        response.original_language,
                        response.overview)
                    tvShowList.add(movie)
                }
                tvShowResults.postValue(tvShowList)
            }

        })

        return tvShowResults
    }

    override fun getTvShowByTitle(tvShowTitle: String): LiveData<Film> {
        val tvShowResult = MutableLiveData<Film>()

        remoteDataSource.getAllTvShows(object : RemoteDataSource.LoadTvShowsCallback {
            override fun onAllTvShowsReceived(tvShowResponses: List<FilmResponse>) {
                for (response in tvShowResponses) {
                    if (response.title == tvShowTitle) {
                        val tvShow = Film(
                            response.title,
                            response.poster,
                            response.genre,
                            response.running_time,
                            response.original_language,
                            response.overview
                        )
                        tvShowResult.postValue(tvShow)
                    }
                }
            }
        })
        return tvShowResult
    }


}