package com.ata.appbundle.data.source

import androidx.lifecycle.LiveData
import com.ata.appbundle.data.source.local.model.Film

interface FilmDataSource {

    fun getAllMovies(): LiveData<List<Film>>
    fun getMovieByTitle(movieTitle: String): LiveData<Film>
    fun getAllTvShows(): LiveData<List<Film>>
    fun getTvShowByTitle(tvShowTitle: String): LiveData<Film>

}