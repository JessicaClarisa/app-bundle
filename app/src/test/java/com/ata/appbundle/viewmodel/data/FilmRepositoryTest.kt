package com.ata.appbundle.viewmodel.data

import androidx.arch.core.executor.testing.InstantTaskExecutorRule

import com.ata.appbundle.data.source.remote.RemoteDataSource
import com.ata.appbundle.utils.DataDummy
import com.ata.appbundle.viewmodel.utils.LiveDataTestUtil

import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito.mock
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.doAnswer

class FilmRepositoryTest {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    private val remote = mock(RemoteDataSource::class.java)
    private val filmRepository = FakeFilmRepository(remote)

    private val movieResponses = DataDummy.generateRemoteDummyMovies()
    private val movieTitle = movieResponses[0].title
    private val tvShowResponses = DataDummy.generateRemoteDummyTVSeries()
    private val tvShowTitle = tvShowResponses[0].title

    @Test
    fun getAllMovies() {
        doAnswer { invocation ->
            (invocation.arguments[0] as RemoteDataSource.LoadMoviesCallback)
                .onAllMoviesReceived(movieResponses)
            null
        }.`when`(remote).getAllMovies(any())
        val film = LiveDataTestUtil.getValue(filmRepository.getAllMovies())
        verify(remote).getAllMovies(any())
        assertNotNull(film)
        assertEquals(movieResponses.size.toLong(), film.size.toLong())
    }

    @Test
    fun getMovieByTitle() {
        doAnswer { invocation ->
            (invocation.arguments[0] as RemoteDataSource.LoadMoviesCallback)
                .onAllMoviesReceived(movieResponses)
            null
        }.`when`(remote).getAllMovies(any())

        val film = LiveDataTestUtil.getValue(filmRepository.getMovieByTitle(movieTitle))

        verify(remote).getAllMovies(any())

        val response = movieResponses[0]

        assertNotNull(film)
        assertEquals(response.title, film.title)
        assertEquals(response.poster, film.poster)
        assertEquals(response.genre, film.genre)
        assertEquals(response.running_time, film.running_time)
        assertEquals(response.original_language, film.original_language)
        assertEquals(response.overview, film.overview)
    }

    @Test
    fun getAllTvShows() {
        doAnswer { invocation ->
            (invocation.arguments[0] as RemoteDataSource.LoadTvShowsCallback)
                .onAllTvShowsReceived(tvShowResponses)
            null
        }.`when`(remote).getAllTvShows(any())
        val film = LiveDataTestUtil.getValue(filmRepository.getAllTvShows())
        verify(remote).getAllTvShows(any())
        assertNotNull(film)
        assertEquals(tvShowResponses.size.toLong(), film.size.toLong())
    }

    @Test
    fun getTvShowByTitle() {
        doAnswer { invocation ->
            (invocation.arguments[0] as RemoteDataSource.LoadTvShowsCallback)
                .onAllTvShowsReceived(tvShowResponses)
            null
        }.`when`(remote).getAllTvShows(any())

        val film = LiveDataTestUtil.getValue(filmRepository.getTvShowByTitle(tvShowTitle))

        verify(remote).getAllTvShows(any())

        val response = tvShowResponses[0]

        assertNotNull(film)
        assertEquals(response.title, film.title)
        assertEquals(response.poster, film.poster)
        assertEquals(response.genre, film.genre)
        assertEquals(response.running_time, film.running_time)
        assertEquals(response.original_language, film.original_language)
        assertEquals(response.overview, film.overview)
    }


}