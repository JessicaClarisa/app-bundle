package com.ata.appbundle.viewmodel.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.ata.appbundle.data.source.FilmRepository
import com.ata.appbundle.data.source.local.model.Film
import com.ata.appbundle.utils.DataDummy
import com.ata.appbundle.viewmodel.TvShowsViewModel
import com.nhaarman.mockitokotlin2.verify
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class TvShowsViewModelTest {
    private lateinit var viewModel: TvShowsViewModel

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Mock
    private lateinit var filmRepository: FilmRepository

    @Mock
    private lateinit var observer: Observer<List<Film>>

    @Before
    fun setUp() {
        viewModel = TvShowsViewModel(filmRepository)
    }

    @Test
    fun getTvShows() {
        val dummyTvShows = DataDummy.generateDummyTVSeries()
        val tvShows = MutableLiveData<List<Film>>()
        tvShows.value = dummyTvShows

        `when`(filmRepository.getAllTvShows()).thenReturn(tvShows)
        val model = viewModel.getTvShows().value
        verify(filmRepository).getAllTvShows()
        assertNotNull(model)
        assertEquals(10, model?.size)

        viewModel.getTvShows().observeForever(observer)
        verify(observer).onChanged(dummyTvShows)
    }
}