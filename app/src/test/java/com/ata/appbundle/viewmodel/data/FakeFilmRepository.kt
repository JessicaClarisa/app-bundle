package com.ata.appbundle.viewmodel.data

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.ata.appbundle.data.source.FilmDataSource
import com.ata.appbundle.data.source.local.model.Film
import com.ata.appbundle.data.source.remote.RemoteDataSource
import com.ata.appbundle.data.source.remote.response.FilmResponse
import java.util.*

class FakeFilmRepository(private val remoteDataSource: RemoteDataSource) : FilmDataSource {

    override fun getAllMovies(): LiveData<List<Film>> {
        val movieResults = MutableLiveData<List<Film>>()

        remoteDataSource.getAllMovies(object : RemoteDataSource.LoadMoviesCallback {
            override fun onAllMoviesReceived(movieResponses: List<FilmResponse>) {
                val movieList = ArrayList<Film>()
                for (i in movieResponses.indices) {
                    val response = movieResponses[i]
                    val movie = Film(response.title,
                        response.poster,
                        response.genre,
                        response.running_time,
                        response.original_language,
                        response.overview)
                    movieList.add(movie)
                }
                movieResults.postValue(movieList)
            }

        })

        return movieResults
    }

    override fun getMovieByTitle(movieTitle: String): LiveData<Film> {
        val movieResult = MutableLiveData<Film>()

        remoteDataSource.getAllMovies(object : RemoteDataSource.LoadMoviesCallback {
            override fun onAllMoviesReceived(movieResponses: List<FilmResponse>) {
                for (i in movieResponses.indices) {
                    val response = movieResponses[i]
                    if (response.title == movieTitle) {
                        val movie = Film(
                            response.title,
                            response.poster,
                            response.genre,
                            response.running_time,
                            response.original_language,
                            response.overview
                        )
                        movieResult.postValue(movie)
                    }
                }
            }
        })
        return movieResult
    }

    override fun getAllTvShows(): LiveData<List<Film>> {
        val tvShowResults = MutableLiveData<List<Film>>()

        remoteDataSource.getAllTvShows(object : RemoteDataSource.LoadTvShowsCallback {
            override fun onAllTvShowsReceived(tvShowResponses: List<FilmResponse>) {
                val tvShowList = ArrayList<Film>()
                for (i in tvShowResponses.indices) {
                    val response = tvShowResponses[i]
                    val movie = Film(response.title,
                        response.poster,
                        response.genre,
                        response.running_time,
                        response.original_language,
                        response.overview)
                    tvShowList.add(movie)
                }
                tvShowResults.postValue(tvShowList)
            }

        })

        return tvShowResults
    }

    override fun getTvShowByTitle(tvShowTitle: String): LiveData<Film> {
        val tvShowResult = MutableLiveData<Film>()

        remoteDataSource.getAllTvShows(object : RemoteDataSource.LoadTvShowsCallback {
            override fun onAllTvShowsReceived(tvShowResponses: List<FilmResponse>) {
                for (i in tvShowResponses.indices) {
                    val response = tvShowResponses[i]
                    if (response.title == tvShowTitle) {
                        val tvShow = Film(
                            response.title,
                            response.poster,
                            response.genre,
                            response.running_time,
                            response.original_language,
                            response.overview
                        )
                        tvShowResult.postValue(tvShow)
                    }
                }
            }
        })
        return tvShowResult
    }


}

