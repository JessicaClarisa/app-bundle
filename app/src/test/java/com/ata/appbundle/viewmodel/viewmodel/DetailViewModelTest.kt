package com.ata.appbundle.viewmodel.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.ata.appbundle.data.source.FilmRepository
import com.ata.appbundle.data.source.local.model.Film
import com.ata.appbundle.utils.DataDummy
import com.ata.appbundle.viewmodel.DetailViewModel
import com.nhaarman.mockitokotlin2.verify
import org.junit.Test

import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class DetailViewModelTest {
    private lateinit var viewModel: DetailViewModel
    private var dummyFilm = DataDummy.generateDummyMovies()[0]
    private val title = dummyFilm.title
    private val type = 1

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Mock
    private lateinit var filmRepository: FilmRepository

    @Mock
    private lateinit var filmObserver: Observer<Film>


    @Before
    fun setUp() {
        viewModel = DetailViewModel(filmRepository)
        viewModel.setTitle(title)
        viewModel.setType(type)
    }

    @Test
    fun getFilm() {
        val film = MutableLiveData<Film>()
        film.value = dummyFilm

        `when`(filmRepository.getMovieByTitle(title)).thenReturn(film)
        val model = viewModel.getDetail().value as Film
        verify(filmRepository).getMovieByTitle(title)
        assertNotNull(model)
        assertEquals(dummyFilm.title, model.title)
        assertEquals(dummyFilm.poster, model.poster)
        assertEquals(dummyFilm.original_language, model.original_language)
        assertEquals(dummyFilm.running_time, model.running_time)
        assertEquals(dummyFilm.genre, model.genre)
        assertEquals(dummyFilm.overview, model.overview)

        viewModel.getDetail().observeForever(filmObserver)
        verify(filmObserver).onChanged(dummyFilm)
    }
}